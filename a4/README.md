> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Jimmy Anderson

### Assignment 4 Requirements:

*Four Parts:*

1. Create a database for eStore
2. Implement database into Microsoft SQL Server(fsuid)
3. Generate SQL reports
4. Chapter Questions (Chp. 14)

#### README.md file should include the following items:

* SQL File Used to Created Database
* Screenshot of Database ERD
* Screenshot of Database Tables Populated With Data

#### Assignment Links:

*A4 Database SQL File:*
[A4 Database SQL File](https://bitbucket.org/jda17d/lis3781/src/master/a4/docs/LIS3781JimmyAndersonA4.sql "A4 Database SQL File")

#### Assignment Screenshots:

| *Screenshot of A4 Business Rules*: |
| :----:  |
| ![A4 Business Rules](img/brules.png) |

| *Screenshot of A4 ERD*: |
| :----:  |
| ![A4 ERD](img/erd.png) |

| *Screenshot of A4 ERD - Microsoft SQL server version(less detailed)*: |
| :----:  |
| ![A4 ERD](img/erd2.png) |

| *Screenshot of Tables - Person/Slsrep/Phone*: | *Screenshot of Tables - Customer/Contact/Order*: |
| :----:        |    :----:   |
| ![Schema/Person/Slsrep](img/tables1.png) | ![Customer/Contact/Order](img/tables2.png) |

| *Screenshot of Tables - Store/Invoice/Vendor/Product*: | *Screenshot of Tables - Order_line/Payment/Product_hist/Srp_hist*: |
| :----:        |    :----:   |
| ![Store/Invoice/Vendor/Product](img/tables3.png) | ![Order_line/Payment/Product_hist/Srp_hist](img/tables4.png) |

#### Tutorial Links:

*A4 Part 1:*
[A4 Part 1 Tutorial](https://youtu.be/acr4eQ610BY "A4 Tutorial Part 1")

*A4 Part 2:*
[A4 Part 2 Tutorial](https://youtu.be/YK242RlPEjc "A4 Tutorial Part 2")

*A4 Part 3:*
[A4 Part 3 Tutorial](https://youtu.be/sB_LCKaE4Ao "A4 Tutorial Part 3")

*A4 Part 4:*
[A4 Part 4 Tutorial](https://youtu.be/skSlheq4LeQ "A4 Tutorial Part 4")