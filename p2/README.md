> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Jimmy Anderson

### Project 2 Requirements:

*Four Parts:*

1. Install / Setup MongoDB
2. Create Free MongoDB Cluster
3. Populate MongoDB cluster with Restaurant Sample Collection Set
4. Provide Screenshot of MongoDB Commands with Populated Collection
5. Chapter Questions (Chapter 16)

#### README.md file should include the following items:

* Screenshot MongoDB Commands with Populated Collection Results

#### Assignment Screenshots:

| *Screenshot of MongoDB Commands with Populated Collection Results*: |
| :----:        |
| ![Project 2](img/p2.png) |

#### Tutorial Links:

*P2 Helper:*
[P2 Helper](https://www.youtube.com/watch?v=QpFcoRVwc18 "P2 Helper")