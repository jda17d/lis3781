public class TemperatureConversion
{
    public static void main(String args[])
    {
        //call static void methods (i.e., no object)
        Methods.getRequirements();
        Methods.convertTemp();
    }
}