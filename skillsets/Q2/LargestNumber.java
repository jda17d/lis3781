import java.util.Scanner; //import statement to allow the program to take user input

public class LargestNumber {
    public static void main(String[] args) {
        //print statements to show name program details
        System.out.println("Developer: Jimmy Anderson");
        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values. \n");

        int num1, num2;

        Scanner scnr = new Scanner(System.in); //create scanner object for user input

        //prompt user to take input
        System.out.print("Enter first integer: ");
        num1 = scnr.nextInt();

        System.out.print("Enter second integer: ");
        num2 = scnr.nextInt();

        System.out.println(); //line for spacing

        //determines larger input
        if(num1 == num2) {
            System.out.printf("Integers are equal.");
        }
        else if (num1 > num2) {
            System.out.printf("%d is larger than %d\n" , num1, num2);
        }
        else {
            System.out.printf("%d is larger than %d\n" , num2, num1);
        }
    }
}