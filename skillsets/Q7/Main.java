public class Main
{
    public static void main(String args[])
    {
        //call static methods (i.e., no object)
        Methods.getRequirements();

        //Java style String[] myArray
        //C++ style String myArray[]
        //call createArray() method in Methods class
        //returns initialized array, array size determined by user
        int[] userArray = Methods.createArray(); //Java style array

        //call generatePseudoRandomNumber() method, passing returned array above
        //prints pseudo-randomly generated numbers, determined by number user inputs
        Methods.generatePseudoRandomNumbers(userArray); //pass array
    }
}