import java.util.Scanner;

public class DecisionStructures
{
    public static void main(String[] args)
    {
        //call static methods (i.e., no object)
        Methods.getRequirements();
        Methods.decisionStructures();
    }
}