

    import java.util.Scanner;

    public class LargestThreeNumbers
    {
        public static void main (String args[])
        {
            //initialize variables
            int num1, num2, num3;
            Scanner input = new Scanner (System.in);

            //explains program
            System.out.println("Developer: Jimmy Anderson");
            System.out.println("Program evaluates largest of three integers.");
            System.out.println("Note: Program checks for integers and non-numeric values.\n");

            //asks user for first numnber while checking if integer
            System.out.print("Please enter first number: " );
            while (!input.hasNextInt())
                {
                    System.out.println("Not valid integer!\n");
                    input.next();
                    System.out.print("Please try again. Enter first number: ");
                }
            num1 = input.nextInt();

            //asks user for second number while checking if integer
            System.out.print("\nPlease enter second number: ");
            while (!input.hasNextInt())
                {
                    System.out.println("Not valid integer!\n");
                    input.next();
                    System.out.print("Please try again. Enter second number: ");
                }
            num2 = input.nextInt();

            //asks user for third number while checking if integer
            System.out.print("\nPlease enter third number: " );
            while (!input.hasNextInt())
                {
                    System.out.println("Not valid integer!\n");
                    input.next();
                    System.out.print("Please try again. Enter third number: ");
                }
            num3 = input.nextInt();

            //displays result if all three are the same
            if (num1 == num2 && num2 == num3)
                {
                    System.out.println("\nAll three numbers are the same.");
                }

            //displays which number was bigger if two are the same
            else if (num1 < num2 && num1 < num3 && num2 == num3)
                {
                    System.out.println("\nSecond and Third are the same/largest.");
                }
            else if (num2 < num1 && num2 < num3 && num1 == num3)
                {
                    System.out.println("\nFirst and Third are the same/largest.");
                }
            else if (num3 < num1 && num3 < num2 && num1 == num2)
                {
                    System.out.println("\nFirst and Second are the same/largest.");
                }
            
            //displays which number was bigger
            else if (num1 > num2 && num1 > num3)
                {
                    System.out.println("\nFirst number is largest.");
                }
            else if (num2 > num3)
                {
                    System.out.println("\nSecond number is largest.");
                }
            else
                {
                    System.out.println("\nThird number is largest.");
                }

        }
    }