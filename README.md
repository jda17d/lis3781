> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
# LIS3781 - Advanced Database Management

## Jimmy Anderson

### LIS3781 Requirements:

*Course Work Links:*

#### Assignments

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Installed AMPPS
    * Provided screenshots of installations
    * Created Bitbucket repo
    * Completed Bitbucket tutorial (bitbucketstationlocations)
    * Provided git command descriptions
    * Created database/erd with 5 records per table
2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Create database through SQL (no workbench)
    * Foward engineer to CCI server and local
    * Create user1 and user2
    * Grant user1 and user2 with specific privileges
    * Provide screenshots of populated tables and SQL code
3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Establish connection to designated tablespace
    * Create database through OracleDB (No ERD) for Estore
    * Populate tables in database with 5 records each
    * Provide screenshots of populated tables and SQL code
4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Establish connection to Microsoft SQL Server
    * Create database for Estore (with ERD)
    * Populate tables in database with 5 - 10 records
    * Provide screenshots of populated tables and ERD
5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Establish connection to Microsoft SQL Server
    * Alter the A4 Estore Database to accomodate new business rules
    * Populate tables with data 5 records min (25 records in sales)
    * Provide screenshots of populated tables and Microsoft SQL Server ERD

#### Projects

6. [P1 README.md](p1/README.md "My A6 README.md file")
    * Create a database to track city courts (locally & server)
    * Provide screenshot of ERD for database
    * Provide screenshot of populated database tables
    * Provide MWB and SQL file
7. [P2 README.md](p2/README.md "My A7 README.md file")
    * Install / Setup MongoDB
    * Create and configure free cluster
    * Import restaurant sample collection set
    * Provide screenshot of MongoDB command with collection results