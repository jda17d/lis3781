> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Jimmy Anderson

### Assignment 3 Requirements:

*Four Parts:*

1. Establish connection to designated tablespace
2. Create database for an Estore in OracleDB
3. Populate database with 5 records each
4. Chapter Questions (Chp. 12)

#### README.md file should include the following items:

* Screenshot of SQL Code
* Screenshots of populated tables in Oracle environment
* Link to A3 SQL file

#### Assignment Screenshots:

| *Screenshot of SQL 1/2*: | *Screenshot of SQL 2/2*: |
| :----:        |    :----:   |
| ![SQL Code 1/2](img/sql1.png) | ![SQL Code 2/2](img/sql2.png) |

| *Screenshot of populated Estore database*: |
| :----:  |
| ![Populated Database](img/oraclepopulated.png) |

| *Screenshot of Estore database table - Customer*: | *Screenshot of Estore database table - Commodity*: | *Screenshot of Estore database table - "Order"*: |
| :----:        |    :----:   |   :----:    |
| ![Customer Table](img/pop1.png) | ![Commodity Table](img/pop2.png) | ![Order Table](img/pop3.png) |

#### Document Links:

*A3 SQL file:*
[A3 SQL file](https://bitbucket.org/jda17d/lis3781/src/master/a3/docs/JimmyAndersonLIS3781A3_sql_code.sql "A3 SQL file")