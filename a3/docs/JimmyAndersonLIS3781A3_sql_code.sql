/*
How Oracle Enforces Data Integrity: Through integrity constraints or triggers.

Integrity Constraints:
- NOT NULL: prohibits a value from being null
- UNIQUE: values must be unique for all rows in the table
- PRIMARY KEY: uniquely identifies each table row
- FOREIGN KEY: requires values in one table to match values in another tables (or may be null). Referential integrity actions:
    Update and Delete No Action
    Delete CASCADE (Note: there in no "update" cascade. Oracle believes PKs should be immutable, that is, never changing.)
    Delete SET NULL
- CHECK: requires value to comply with specified condition

For numeric columns, specify attribute as "number" data type (case-insensitive).
Example: column_name NUMBER

Optionally, you can also specify precision (total number of digits) and scale (number of digits to right of decimal point):
column_name NUMBER (precision, scale)

If precision not specified, column stores values as given. If no scale specified, scale is zero.
Oracle guarantees portability of numbers with a precision equal to or less than 38 digits.

Can specify scale and precision:
column_name NUMBER (*, scale)
In this case, precision is 38, and whatever specified scale is maintained.
When specifying numeric fields, go idea to specify precision and scale. Provides extra integrity checking on input, as well as intuitive documentation.
*/

-- ####### BEGIN #######
-- 1. Connect to *your* schema
-- Canvas > Home > Notes > Oracle SQL Developer Login

/*
By default, SQL Plus treats '&' as a special character that begins a substitution string.
This can cause problems when running scripts that happen to include '&' for other reasons:

Example:
insert into customers (customer_name) values ('Marks & Spencers Ltd');

If you know your script includes (or may include) data containing '&' characters,
and do not want substitution behaviorm use Set DEFINE OFF, to switch off (supress) substitution prompts while running script.
*/

SET DEFINE OFF -- (Note: don't include semi-colon at end of line!) Also, test with first record in commodity table below

--Notes:
-- CASCADE CONSTRAINTS: Specify CASCADE CONSTRAINTS to drop all referential integrity constraints that refer to primary and unique keys in dropped table.
-- If clause omitted, and such referential integrity constraints exists, database returns an error and does *NOT* drop table.

-- PURGE: unless PURGE clause specified, DROP TABLE statement does *not* release memory back to tablespace for use by other objects.
-- Moreover, space continues to count toward user's space quota.

--Notes: DO *NOT* USE DROP/CREATE DATABASE COMMAND, AS YOUR TABLES ARE CREATED IN YOUR OWN TABLESPACE--*NOT* DATABASE!
--Just connect to your schema (as per Step #1 above) and create following tables:
-- 2. Create estore tables
DROP SEQUENCE seq_cus_id; -- see Oracle documentation for auto increment
Create sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;
-- create SEQUENCE seq_cus_id; -- default: starts with 1 and increments by 1

/*
Note:
VARCHAR is reserved by Oracle to support distinction between NULL and empty string in future, as ANSI standard prescribes.
VARCHAR2 does not distinguish between a NULL and empty string, and never will.
If you rely on empty string and NULL being the same thing, you should use VARCHAR2 .
Reference: https://stackoverflow.com/questions/1171196/what-is-the-difference-between-varchar-and-varchar2-in-oracle

Oracle's documentation:
VARCHAR Datatype
The VARCHAR datatype is synonymous with the VARCHAR2 datatype.
To avoid possible changes in behavior, always use the VARCHAR2 datatype to store variable-length character strings.
Reference: https://docs.oracle.com/cd/B19306_01/server.102/b14220/datatype.htm#sthref3784

Bottom-line:
Currently they're the same!
*HOWEVER*, VARCHAR *may* one day in the future have a different useage than VARCHAR2.
Be safe--use VARCHAR2.
*/

drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
    cus_id number(3,0) not null, -- max value 999
    cus_fname varchar2(15) not null,
    cus_lname varchar2(30) not null,
    cus_street varchar2(30) not null,
    cus_city varchar2(30) not null,
    cus_state char(2) not null,
    cus_zip number(9) not null, -- equivalent to number(9,0)
    cus_phone number(10) not null,
    cus_email varchar2(100),
    cus_balance number(7,2), -- max value 99999.99
    cus_notes varchar2(255),
    CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);

DROP SEQUENCE seq_com_id; -- for auto increment
Create sequence seq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
    com_id number not null,
    com_name varchar2(20),
    com_price NUMBER(8,2) NOT NULL,
    cus_notes varchar2(255),
    CONSTRAINT pk_commodity PRIMARY KEY(com_id),
    CONSTRAINT uq_com_name UNIQUE(com_name) -- Note: case-sensitive by default!
);

-- On your own: Research how to enforce case-insensitive unique constraint on commodity name

/*
Note:
Unique constraint (above) enforces data integrity--implicitly created unique index as well.
Unique index is for uniqueness *and* data retrieval performance:
Following statement explicitly creates unique index named ux_com_name for com_name attribute of commodity table:
CREATE UNIQUE INDEX ux_com_name on commodity(com_name);

Indexes can be unique or non-unique:
Unique indexes: guarantee that no two rows of a table have duplicate values in column (or columns).
Non-unique indexes: do not impose this restriction--though, *do* provide data retrieval performance

Can create non-unique indexes explicitly (i.e., outside of integrity constraints, strictly for retrieval performance):
Following statement creates index named idx_com_name for com_name attribute of commodity table:
CREATE INDEX idx_com_name on commodity(com_name);

Display names of all tables constraints:
Must query data dictionary, specifically USER_CONS_COLUMNS view to see table columns and corresponding constraints:

SELECT * FROM user_cons_columns
WHERE table_name = '<your table name>';

NOTE: UNLESS TABLE SPECIFICALLY CREATED WITH LOWER CASE NAME (I.E., USING DOUBLE QUOTES), TABLE NAME WILL DEFAULT TO UPPER CASE!

Example:
SELECT * FROM user_cons_columns
WHERE table_name = 'commodity'; -- will *not* work!

SELECT * FROM user_cons_columns
WHERE table_name = 'COMMODITY'; -- will work!

Legend: P, R, and C: 'primary key,' 'referential integrity,' and 'check constraint' (e.g., NOT NULL)

More information about table constraints, query USER_CONSTRAINTS view:
SELECT * FROM user_constraints
WHERE table_name = '<your table name>';

More information about specific constraint, query USER_CONSTRAINTS view:
SELECT * FROM user_constraints
WHERE table_name = '<your table name>'
    AND constraint_name = '<your constraint name>';

https://docs.oracle.com/cd/B28359_01/server.111/b28318/datadict.htm#CNCPT002

-- Short constraints list:
-- Note: SYS_C is system-generated name for constraint not explicitly named. Here: not-null check.
select constraint_name, constraint_type
from user_constraints
where table_name = 'COMMODITY';

-- More constraint information:
select uc.constraint_name, uc.constraint_type, ucc.column_name, ucc.position
from user_constraints uc
join user_cons_columns ucc on ucc.constraint_name = uc.constraint_name
where uc.table_name = 'COMMODITY';

-- Short indexes list:
select index_name, uniqueness
from user_indexes
where table_name = 'COMMODITY';
*/

DROP SEQUENCE seq_ord_id; -- for auto increment
Create sequence seq_ord_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

-- Demo purposes: Quoted identifiers can be reserved words (e.g., order), although this is *not* recommended
drop table "order" CASCADE CONSTRAINTS PURGE;
CREATE TABLE "order"
(
    ord_id number(4,0) not null, -- max value 9999 (permitting only integers, no decimals)
    cus_id number,
    com_id number,
    ord_num_units number(5,0) NOT NULL, -- max value 99999 (permitting only integers, no decimals)
    ord_total_cost number(8,2) NOT NULL,
    ord_notes varchar2(255),
    CONSTRAINT pk_order PRIMARY KEY(ord_id),
    CONSTRAINT fk_order_customer
    FOREIGN KEY (cus_id)
    REFERENCES customer(cus_id),
    CONSTRAINT fk_order_commodity
    FOREIGN KEY (com_id)
    REFERENCES commodity(com_id),
    CONSTRAINT check_unit CHECK(ord_num_units > 0),
    CONSTRAINT check_total CHECK(ord_total_cost > 0)
);

-- Oracle NEXTVAL function used to retrieve next value in sequence
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Tom', 'Filing', '324 Taco Dr.', 'Tallahassee', 'FL', 32311, 8505551234, 'tomfiling@gmail.com', 77325.99, 'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Sarah', 'Thompson', '3463 Chicken St.', 'Miami', 'FL', 32301, 8505554321, 'sarahthompson@aol.com', 352.52, 'new customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Kate', 'Smith', '234 Orange Av.', 'Tampa', 'FL', 63463, 8505559876, 'katesmith@comcast.net', 532.45, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Bill', 'Dozer', '856 Drive Dr.', 'Jacksonville', 'FL', 43342, 8505556789, 'billdozer@wow.com', 3466.43, 'returning customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Mike', 'Tyson', '7523 Road Rd.', 'Dallas', 'TX', 85394, 7235551298, 'miketyson@yahoo.com', NULL, NULL);
commit;

-- Note: Oracle does *not* autocommit by default! DML STATEMENTS WILL ONLY LAST FOR THE SESSION!
-- When forgetting to commit DML statements--for example, with inserts, selecting a table will display "no rows selected"!

INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD & Player', 119.99, 'sony');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Chips', 2.50, 'low fat');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Boggle', 23.45, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Licorice', 1.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Tylenol', 2.86, 'childrens');
commit;

INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 50, 200, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 1, 25, 100, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 5, 63, 357, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 4, 3, 34, 223, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 6, 46, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 4, 25, 100, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 4, 1, 50, 200, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 2, 3, 23, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 48, 278, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 4, 82, 643, NULL);
commit;

select * from customer;
select * from commodity;
select * from "order";