-- MS SQL Server
/*
NOTES: Tables *must* include the following constraints and defaults:
*   per_ssn: must be unique (see indexes/keys), and SHA2_512 hashed
*   per_gender: m or f
*   per_type: c or s
Example: ([per_gender]= 'f' OR [per_gender]= 'm')
*   state: default = FL
*   zip: require entries in zip column to be 9 digits
Example: ([per_zip] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
*   phone num: requires entries in phone column to be 10 digits
Example: ([phn_num] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
*   phone type: home, cell, work, fax
Example: ([phn_type]='f' OR [phn_type]='w' OR [phn_type]='c' OR [phn_type]='h')
*   *all* numeric values: >= 0
Example: ([srp_yr_sales_goal] >= (0))

3.  FK: Must require ON DELETE CASCADE, ON UPDATE CASCADE

Question: Why use dbo.?
Answer:
From my research, even if the SQL code doesn't have to use the fully qualified name (e.q., dbo.customer),
evidently, there is a slight performance gain in doing so, because the optimizer doesn't have to look up the schema.
And, it is considered a best practice.
http://wwww.sqlteam.com/article/understanding-the-difference-between-owners-and-schemas-in-sql-Server
*/

-- (not the same, but) similar to SHOW WARNINGS;
SET ANSI_WARNINGS ON;
GO

-- avoids error that user kept db connection open
use master;
GO

-- drop existing database if it exists (us *your* username)
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jda17d')
DROP DATABASE jda17d;
GO

-- create database if not exists (use *your username)
IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'jda17d')
CREATE DATABASE jda17d;
GO

use jda17d;
GO

-- ***BE SURE*** to create/populate tables in correct order--parent tables then child tables!

-- drop table if exists
-- N=subsequent string may be in Unicode (makes it portable to use with Unicode characters)
-- U=only look for objects with this name that are tables
-- *be sure* to use dbo. before *all* table references

-- ------------------------------------------------------------
-- Table person
-- ------------------------------------------------------------
IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person
(
    per_id SMALLINT not null identity(1,1),
    per_ssn binary(64) NULL,
    per_salt binary(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m', 'f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL DEFAULT 'FL',
    per_zip int NOT NULL check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NULL,
    per_type CHAR(1) NOT NULL CHECK (per_type IN('c', 's')),
    per_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    -- make sure SSNs and State IDs are unique
    CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO

-- ---------------------------------------------------------
-- Table phone
-- ---------------------------------------------------------
IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone
(
    phn_id SMALLINT NOT NULL identity(1,1),
    per_id SMALLINT NOT NULL,
    phn_num bigint NOT NULL check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) NOT NULL CHECK (phn_type IN('h', 'c', 'w', 'f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    CONSTRAINT fk_phone_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- ----------------------------------------------------------
-- Table customer
-- ----------------------------------------------------------
IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer
(
    per_id SMALLINT not null,
    cus_balance decimal(7,2) NOT NULL check (cus_balance >= 0),
    cus_total_sales decimal(7,2) NOT NULL check (cus_total_sales >= 0),
    cus_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- -------------------------------------------
-- Table slsrep
-- -------------------------------------------
IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep
(
    per_id SMALLINT not null,
    srp_yr_sales_goal decimal(8,2) NOT NULL check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) NOT NULL check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) NOT NULL check (srp_ytd_comm >= 0),
    srp_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_slsrep_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- -----------------------------------------
-- Table srp_hist
-- -----------------------------------------
IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
    sht_id SMALLINT not null identity(1,1),
    per_id SMALLINT not null,
    sht_type char(1) not null CHECK (sht_type IN('i', 'u', 'd')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) NOT NULL check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) NOT NULL check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) NOT NULL check (sht_yr_total_comm >= 0),
    sht_notes VARCHAR(45) NULL,
    PRIMARY KEY (sht_id),

    CONSTRAINT fk_srp_hist_slsrep
    FOREIGN KEY (per_id)
    REFERENCES dbo.slsrep (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- --------------------------------------------
-- Table contact
-- --------------------------------------------
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact
(
    cnt_id int NOT NULL identity(1,1),
    per_cid smallint NOT NULL,
    per_sid smallint NOT NULL,
    cnt_date datetime NOT NULL,
    cnt_notes varchar(255) NULL,
    PRIMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
    FOREIGN KEY (per_cid)
    REFERENCES dbo.customer (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

-- see note below
    CONSTRAINT fk_contact_slsrep
    FOREIGN KEY (per_sid)
    REFERENCES dbo.slsrep (per_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

/*
NOTE: Cannot have CASCADE paths on both fks in CONTACT, because records in CONTACT table are linked with CUSTOMER records,
notes
notes
notes
notes
*/

-- ----------------------------------------------
-- Table [order]
-- must use delimiter [] for reserved words (e.g., order)
-- ----------------------------------------------
IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
    ord_id int NOT NULL identity(1,1),
    cnt_id int NOT NULL,
    ord_placed_date DATETIME NOT NULL,
    ord_filled_date DATETIME NULL,
    ord_notes VARCHAR(255) NULL,
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact (cnt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- -------------------------------------------
-- Table region
-- -------------------------------------------
IF OBJECT_ID (N'dbo.region', N'U') IS NOT NULL
DROP TABLE dbo.region;
GO

CREATE TABLE region
(
    reg_id TINYINT NOT NULL identity(1,1),
    reg_name CHAR(1) NOT NULL, --n,e,s,w,c (north, east, south, west, central)
    reg_notes VARCHAR(255) NULL,
    PRIMARY KEY (reg_id)
);
GO

-- ------------------------------------------------------
-- Table state
-- ------------------------------------------------------
IF OBJECT_ID (N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state;
GO

CREATE TABLE dbo.state
(
    ste_id TINYINT NOT NULL identity(1,1),
    reg_id TINYINT NOT NULL,
    ste_name CHAR(2) NOT NULL DEFAULT 'FL',
    ste_notes VARCHAR(255) NULL,
    PRIMARY KEY (ste_id),

    CONSTRAINT fk_state_region
    FOREIGN KEY (reg_id)
    REFERENCES dbo.region (reg_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ---------------------------------------------------
-- Table city
-- ---------------------------------------------------
IF OBJECT_ID(N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city;
GO

CREATE TABLE dbo.city
(
    cty_id SMALLINT NOT NULL identity(1,1),
    ste_id TINYINT NOT NULL,
    cty_name VARCHAR(30) NOT NULL,
    cty_notes VARCHAR(255) NULL,
    PRIMARY KEY (cty_id),

    CONSTRAINT fk_city_state
    FOREIGN KEY (ste_id)
    REFERENCES dbo.state (ste_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- -------------------------------------------------
-- Table store
-- -------------------------------------------------
IF OBJECT_ID(N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
    str_id SMALLINT NOT NULL identity(1,1),
    cty_id SMALLINT NOT NULL,
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL,
    str_zip int NOT NULL check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint NOT NULL check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL,
    str_url VARCHAR(100) NOT NULL,
    str_notes VARCHAR(255) NULL,
    PRIMARY KEY (str_id),

    CONSTRAINT fk_store_city
    FOREIGN KEY (cty_id)
    REFERENCES dbo.city (cty_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ---------------------------------------------------
-- Table invoice
-- ---------------------------------------------------
IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice
(
    inv_id int NOT NULL identity(1,1),
    ord_id int NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_date DATETIME NOT NULL,
    inv_total DECIMAL(8,2) NOT NULL check (inv_total >= 0),
    inv_paid bit NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY (inv_id),

    -- create 1:1 relationship with order by making ord_id unique
    CONSTRAINT ux_ord_id unique nonclustered (ord_id ASC),

    CONSTRAINT fk_invoice_order
    FOREIGN KEY (ord_id)
    REFERENCES dbo.[order] (ord_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_invoice_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- ----------------------------------------------
-- Table payment
-- ----------------------------------------------
IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment
(
    pay_id int NOT NULL identity(1,1),
    inv_id int NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL check (pay_amt >= 0),
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
    FOREIGN KEY (inv_id)
    REFERENCES dbo.invoice (inv_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- -------------------------------------------
-- Table vendor
-- -------------------------------------------
IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor
(
    ven_id SMALLINT NOT NULL identity(1,1),
    ven_name VARCHAR(45) NOT NULL,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL DEFAULT 'FL',
    ven_zip int NOT NULL check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint NOT NULL check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NULL,
    ven_url VARCHAR(100) NULL,
    ven_notes VARCHAR(255) NULL,
    PRIMARY KEY (ven_id)
);

-- ---------------------------------------------------------
-- Table product
-- ---------------------------------------------------------
IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product
(
    pro_id SMALLINT NOT NULL identity(1,1),
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NULL,
    pro_weight FLOAT NOT NULL check (pro_weight >= 0),
    pro_qoh SMALLINT NOT NULL check (pro_qoh >= 0),
    pro_cost DECIMAL(7,2) NOT NULL check (pro_cost >= 0),
    pro_price DECIMAL(7,2) NOT NULL check (pro_price >= 0),
    pro_discount DECIMAL(3,0) NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_vendor
    FOREIGN KEY (ven_id)
    REFERENCES dbo.vendor (ven_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- ----------------------------------------------------
-- Table product_hist
-- ----------------------------------------------------
IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist
(
    pht_id int NOT NULL identity(1,1),
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL check (pht_cost >= 0),
    pht_price DECIMAL(7,2) NOT NULL check (pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),

    CONSTRAINT fk_product_hist_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- ---------------------------------------------------
-- Table order_line
-- --------------------------------------------------
IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line
(
    oln_id int NOT NULL identity(1,1),
    ord_id int NOT NULL,
    pro_id SMALLINT NOT NULL,
    oln_qty SMALLINT NOT NULL check (oln_qty >= 0),
    oln_price DECIMAL(7,2) NOT NULL check (oln_price >= 0),
    oln_notes VARCHAR(255) NULL,
    PRIMARY KEY (oln_id),

    -- must use delimiters [] on reserved words (e.g., order)
    CONSTRAINT fk_order_line_order
    FOREIGN KEY (ord_id)
    REFERENCES dbo.[order] (ord_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_order_line_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- --------------------------------------------------------
-- Table time
-- --------------------------------------------------------
IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
GO

CREATE TABLE dbo.time
(
    tim_id INT NOT NULL identity(1,1),
    tim_yr SMALLINT NOT NULL, --2 byte integer (no YEAR data type in MS SQL Server)
    tim_qtr TINYINT NOT NULL, -- 1 - 4
    tim_month TINYINT NOT NULL, -- 1 - 12
    tim_week TINYINT NOT NULL, --1 - 52
    tim_day TINYINT NOT NULL, --1 - 7
    tim_time TIME NOT NULL, -- based on 24-hour clock
    tim_notes VARCHAR(255) NULL,
    PRIMARY KEY (tim_id)
);
GO

-- --------------------------------------------------------------
-- Table sale
-- --------------------------------------------------------------
IF OBJECT_ID(N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale
(
    pro_id SMALLINT NOT NULL,
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL,
    tim_id INT NOT NULL,
    sal_qty SMALLINT NOT NULL,
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),

    -- make sure combination of time, contact, store, and product are unique
    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
    unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
    FOREIGN KEY (tim_id)
    REFERENCES dbo.time (tim_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_contact
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact (cnt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

SELECT * FROM information_schema.tables;

-- coverts to binary
SELECT HASHBYTES('SHA2_512', 'test');

-- length 64 bytes
SELECT len(HASHBYTES('SHA2_512', 'test'));

-- -------------------------------------------------------
-- Data for table person: 5 sales reps: 1 - 5, 10 customers: 6 - 15
-- NOTE: do *not* include attribute name or value for auto increment attributes (i.e., pks)
-- *Must* use initial placeholder values for per_ssn, because of unique constraint. Will be replaced by stored procedures values!
-- -------------------------------------------------------
INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1,NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', 's', NULL),
(2,NULL, 'Bruce', 'Wayne', 'm', '1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', 983208440, 'bwayne@knology.net', 's', NULL),
(3,NULL, 'Peter', 'Parker', 'm', '1988-09-12', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', 's', NULL),
(4,NULL, 'Jane', 'Thompson', 'f', '1978-05-08', '13563 Ocean View Drive', 'Seattle', 'WA', 132084409, 'jthompson@gmail.com', 's', NULL),
(5,NULL, 'Debra', 'Steele', 'f', '1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', 's', NULL),
(6,NULL, 'Tony', 'Smith', 'm', '1972-05-04', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', 'c', NULL),
(7,NULL, 'Hank', 'Pymi', 'm', '1980-08-28', '2355 Brown Street', 'Cleveland', 'OH', 822348890, 'hpym@aol.com', 'c', NULL),
(8,NULL, 'Bob', 'Best', 'm', '1992-02-10', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', 'c', NULL),
(9,NULL, 'Sandra', 'Smith', 'f', '1990-01-26', '87912 Lawrence Ave', 'Atlanta', 'GA', 672348890, 'sdole@gmail.com', 'c', NULL),
(10,NULL, 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', 'c', NULL),
(11,NULL, 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Avenue', 'Miami', 'FL', 342219932, 'acurry@gmail.com', 'c', NULL),
(12,NULL, 'Diana', 'Price', 'f', '1980-08-22', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', 'c', NULL),
(13,NULL, 'Adam', 'Smith', 'm', '1995-01-31', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', 'c', NULL),
(14,NULL, 'Judy', 'Sleen', 'f', '1970-03-22', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', 'c', NULL),
(15,NULL, 'Bill', 'Neiderheim', 'm', '1982-06-13', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', 'c', NULL);
GO

select * from dbo.person;

-- ----------------------------------------------------------
-- Data for table slsrep
-- ----------------------------------------------------------
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 80000, 35000, 3500, NULL),
(3, 150000, 84000, 9650, 'Great salesperson!'),
(4, 125000, 87000, 15300, NULL),
(5, 98000, 43000, 8750, NULL);

SELECT * from dbo.slsrep;

-- ------------------------------------------------------
-- Data for table customer
-- ------------------------------------------------------
INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, 'Customer always pays on time.'),
(9, 981.73, 1672.38, 'High balance.'),
(10, 541.23, 782.57, NULL),
(11, 251.02, 13782.96, 'Good customer.'),
(12, 582.67, 963.12, 'Previously paid in full.'),
(13, 121.67, 1057.45, 'Recent customer.'),
(14, 765.43, 6789.42, 'Buys bulk quantities.'),
(15, 304.39, 456.81, 'Has not purchased recently.');

SELECT * from dbo.customer;
-- ----------------------------------------------------------
-- Data for table contact
-- ----------------------------------------------------------
INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1, 6, '1999-01-01', NULL),
(2, 6, '2001-09-29', NULL),
(3, 7, '2002-08-15', NULL),
(2, 7, '2002-09-01', NULL),
(4, 7, '2004-01-05', NULL),
(5, 8, '2004-02-28', NULL),
(4, 8, '2004-03-03', NULL),
(1, 9, '2004-04-07', NULL),
(5, 9, '2004-07-29', NULL),
(3, 11, '2005-05-02', NULL),
(4, 13, '2005-06-14', NULL),
(2, 15, '2005-07-02', NULL);

SELECT * from dbo.contact;

-- -----------------------------------------------------------
-- Data for table order
-- -----------------------------------------------------------
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-19', '2005-07-28', NULL),
(3, '2011-07-01', '2011-07-06', NULL),
(4, '2009-12-24', '2010-01-05', NULL),
(5, '2008-09-21', '2008-11-26', NULL),
(6, '2009-04-17', '2009-04-30', NULL),
(7, '2010-05-31', '2010-06-07', NULL),
(8, '2007-09-02', '2007-09-16', NULL),
(9, '2011-12-08', '2011-12-23', NULL),
(10, '2012-02-29', '2012-05-02', NULL);

select * from dbo.[order];

-- --------------------------------------------------
-- Data for table region
-- --------------------------------------------------
INSERT INTO region
(reg_name, reg_notes)
VALUES
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL);
GO

select * from dbo.region;

-- --------------------------------------------------
-- Data for table state
-- --------------------------------------------------
INSERT INTO state
(reg_id, ste_name, ste_notes)
VALUES
(1, 'MI', NULL),
(3, 'IL', NULL),
(4, 'WA', NULL),
(5, 'FL', NULL),
(2, 'TX', NULL);
GO

select * from dbo.state;

-- --------------------------------------------------
-- Data for table city
-- --------------------------------------------------
INSERT INTO city
(ste_id, cty_name, cty_notes)
VALUES
(1, 'Detroit', NULL),
(2, 'Aspen', NULL),
(2, 'Chicago', NULL),
(3, 'Clover', NULL),
(4, 'St. Louis', NULL);
GO

select * from dbo.city;

-- --------------------------------------------------
-- Data for table store
-- --------------------------------------------------
INSERT INTO dbo.store
(cty_id, str_name, str_street, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
(2, 'Walgreens', '14567 Walnut Ln', 475315690, 3127658127, 'info@walgreens.com', 'http://www.walgreens.com', NULL),
(3, 'CVS', '572 Casper Rd', 505231519, 3128926534, 'help@cvs.com', 'http://www.cvs.com', 'Rumor of merger'),
(4, 'Lowes', '81309 Catapult Ave', 802345671, 9017653421, 'sales@lowes.com', 'http://www.lowes.com', NULL),
(5, 'Walmart', '14567 Walnut Ln', 387563628, 8722718923, 'info@walmart.com', 'http://www.walmart.com', NULL),
(1, 'Dollar General', '47583 Davison Rd', 482983456, 3137583492, 'ask@dollargeneral.com', 'http://www.dollargeneral.com', 'recently sold property');

select * from dbo.store;

-- ---------------------------------------------------------------------
-- Data for table invoice
-- ---------------------------------------------------------------------
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(5, 1, '2001-05-03', 58.32, 0, NULL),
(4, 1, '2006-11-11', 100.59, 0, NULL),
(1, 1, '2010-09-16', 57.34, 0, NULL),
(3, 2, '2011-01-10', 99.32, 1, NULL),
(2, 3, '2008-06-24', 1109.67, 1, NULL),
(6, 4, '2009-04-20', 239.83, 0, NULL),
(7, 5, '2010-06-05', 537.29, 0, NULL),
(8, 2, '2007-09-09', 644.21, 1, NULL),
(9, 3, '2011-12-17', 934.12, 1, NULL),
(10, 4, '2012-03-18', 27.45, 0, NULL);

select * from dbo.invoice;

-- -----------------------------------------------
-- Data for table vendor
-- -----------------------------------------------
INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '531 Dolphin Run', 'Orlando', 'FL', '344761234', '7641238543', 'sales@sysco.com', 'http://www.sysco.com', NULL),
('General Electric', '100 Happy Trails Dr.', 'Boston', 'MA', '123458743', '2134569641', 'support@ge.com', 'http://www.ge.com', 'Very good turnaround'),
('Cisco', '300 Cisco Dr.', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', NULL),
('Goodyear', '100 Goodyear Dr.', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', 'Competing well with Firestone.'),
('Snap-On', '42185 Magenta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@snapon.com', 'http://www.snap-on.com', 'Good quality tools!');

SELECT * from dbo.vendor;

-- ----------------------------------------------------------
-- Data for table product
-- ----------------------------------------------------------
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer', '', 2.5, 45, 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, 'screwdriver', '', 1.8, 120, 1.99, 3.49, NULL, NULL),
(4, 'pail', '16 Gallon', 2.8, 48, 3.89, 7.99, 40, NULL),
(5, 'cooking oil', 'Peanut oil', 15, 19, 19.99, 28.99, NULL, 'gallons'),
(3, 'frying pan', '', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale.');

SELECT * from dbo.product;

-- ---------------------------------------------------------
-- Data for table order_line
-- ---------------------------------------------------------
INSERT INTO dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1, 2, 10, 8.0, NULL),
(2, 3, 7, 9.88, NULL),
(3, 4, 3, 6.99, NULL),
(5, 1, 2, 12.76, NULL),
(4, 5, 13, 58.99, NULL);

SELECT * from dbo.order_line;

-- -------------------------------------------------
-- Data for table payment
-- -------------------------------------------------
INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(5, '2008-07-01', 5.99, NULL),
(4, '2010-09-28', 4.99, NULL),
(1, '2008-07-23', 8.75, NULL),
(3, '2010-10-31', 19.55, NULL),
(2, '2011-03-29', 32.5, NULL),
(6, '2010-10-03', 20.00, NULL),
(8, '2008-08-09', 1000.00, NULL),
(9, '2009-01-10', 103.68, NULL),
(7, '2008-03-15', 25.00, NULL),
(10, '2007-05-12', 40.00, NULL),
(4, '2007-05-22', 9.33, NULL);

SELECT * from dbo.payment;

-- --------------------------------------------------------
-- -- Data for table phone
-- -------------------------------------------------
INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(2, 8505091234, 'c', 'Never answers'),
(5, 8908932803, 'w', 'Never shuts up'),
(8, 7238578393, 'h', 'Too much information'),
(10, 4372985723, 'f', 'B-O-R-I-N-G!!'),
(14, 3257239573, 'c', 'Best customer!');

SELECT * from dbo.phone;


-- ---------------------------------------------------------
-- Data for table product_hist
-- ---------------------------------------------------------
INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, NULL, NULL),
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, NULL),
(4, '2006-05-06 18:09:04', 19.99, 28.99, NULL, 'gallons'),
(5, '2006-05-07 15:07:29', 8.45, 13.99, 50, 'Currently 1/2 price sale.');

SELECT * from dbo.product_hist;

-- ---------------------------------------------------
-- Data for table time
-- ---------------------------------------------------
INSERT INTO time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(2010, 4, 12, 49, 4, '08:34:21', NULL),
(1999, 4, 12, 52, 5, '05:21:34', NULL),
(2011, 3, 8, 36, 1, '09:32:18', NULL),
(2001, 3, 7, 27, 2, '23:56:32', NULL),
(2008, 1, 1, 5, 4, '04:22:36', NULL),
(2010, 2, 4, 14, 5, '02:49:11', NULL),
(2014, 1, 2, 8, 2, '12:27:14', NULL),
(2013, 3, 9, 38, 4, '10:12:28', NULL),
(2012, 4, 11, 47, 3, '22:36:22', NULL),
(2014, 2, 6, 23, 3, '19:07:10', NULL);
GO

/*
'2008-05-11'
'2010-12-02'
'1999-12-24'
'2011-08-29'
'2001-07-3'
'2008-01-31'
'2010-04-02'
'2014-02-18'
'2013-09-19'
'2012-11-14'
'2014-06-04'

-- parsing date into integers for quarter, month, week, and day
select datepart(q,'2008-05-11);
select datepart(m,'2008-05-11);
select datepart(ww,'2008-05-11);
select datepart(dw,'2008-05-11);

-- research the following MS SQL Server date features:
datepart(), DATENAME(), and SET DATEFIRST 7;
*/

select * from dbo.time;

-- ----------------------------------------------------
-- Data for table dale
-- ----------------------------------------------------
INSERT INTO dbo.sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1, 5, 5, 3, 20, 9.99, 199.8, NULL),
(2, 4, 6, 2, 5, 5.99, 29.95, NULL),
(3, 3, 4, 1, 30, 3.99, 119.7, NULL),
(4, 5, 1, 5, 15, 18.99, 284.85, NULL),
(5, 1, 2, 4, 10, 11.99, 71.94, NULL),
(4, 3, 6, 7, 5, 5.99, 29.95, NULL),
(3, 4, 4, 8, 30, 3.99, 119.7, NULL),
(2, 3, 1, 9, 15, 18.99, 284.85, NULL),
(1, 2, 2, 10, 25, 4.99, 71.94, NULL),
(1, 5, 3, 11, 10, 32.99, 119.9, NULL),
(2, 4, 6, 1, 5, 12.99, 97.95, NULL),
(3, 3, 4, 3, 25, 16.99, 76.92, NULL),
(4, 2, 3, 4, 10, 3.99, 82.92, NULL),
(5, 5, 2, 5, 15, 6.99, 34.63, NULL),
(4, 1, 1, 6, 20, 17.99, 32.8, NULL),
(3, 2, 5, 7, 5, 74.99, 35.93, NULL),
(2, 4, 2, 8, 30, 23.99, 235.93, NULL),
(1, 3, 3, 9, 15, 54.99, 353.96, NULL),
(1, 1, 4, 10, 10, 3.99, 36.60, NULL),
(2, 4, 5, 11, 5, 7.99, 62.36, NULL),
(3, 5, 2, 2, 20, 34.99, 364.70, NULL),
(4, 2, 6, 3, 25, 4.99, 43.47, NULL),
(5, 4, 3, 4, 20, 5.99, 324.94, NULL),
(4, 3, 4, 5, 10, 32.99, 46.92, NULL),
(3, 1, 5, 6, 15, 53.99, 534.96, NULL);
GO

select * from dbo.sale;

-- ----------------------------------------------------
-- Data for table srp_hist
-- ----------------------------------------------------
INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(4, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);

SELECT * from dbo.srp_hist;

-- get year only from srp_hist table
select year(sht_date) from dbo.srp_hist;
GO

-- Populate person table with hashed and salted SSN numbers. *MUST* include salted value in DB!
IF OBJECT_ID (N'dbo.CreatePersonSSN8', N'U') IS NOT NULL
DROP PROC dbo.CreatePersonSSN8;
GO

CREATE PROC dbo.CreatePersonSSN8
AS
BEGIN

    DECLARE @salt binary(64);
    DECLARE @ran_num int;
    DECLARE @ssn binary(64);
    DECLARE @x INT, @y INT;
    SET @x = 1;

-- dynamically set loop ending value (total number of persons)
SET @y=(select count(*) from dbo.person);
-- select @y; -- display number of persons (only for testing)

    WHILE (@x <= @y)
    BEGIN
    -- give each person a unique randomzied salt, and hashed and salted randomized SSN.
    -- Note: this demo is *only* for showing how to include salted and hashed randomized values for testing purposes!
    -- function returns a cryptographic, randomly-generated hexadecimal number with length of specified number of bytes
    -- https://docs.microsoft.com/en-us/sql/t-sql/functions/crypt-gen-random-transact-sql?view=sql-server-2017
    SET @salt=CRYPT_GEN_RANDOM(64);
    SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive (see link below)
    SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));
    -- select @salt, len(@salt), @ran_num, len(@ran_num), @ssn, len(@ssn); -- only for testing values

    -- RAND([N]): Returns random floating-point value v in the range 0 <= v < 1.0
    -- Documentation: https://wwtechonthenet.com/sql_server/functions/rand.php
    -- randomize ssn between 111111111 - 999999999 (note: using value 000000000 for ex. 4 below)
    update dbo.person
    set per_ssn=@ssn, per_salt=@salt
    where per_id=@x;

    SET @x = @x + 1;

    END;

END;
GO

exec dbo.CreatePersonSSN8;
GO

SELECT * from dbo.person;
SELECT * from dbo.slsrep;
SELECT * from dbo.customer;

/*
SYSTEM_USER presents you with the credentials used to run the query.
This is important to establish which permissions were active.

ORIGINAL_LOGIN is giving you the user with which the connection was established.
This is also important information.

SYSTEM_USER
Pro: you can see with which permissions a query was executed
Con: you don't know with which permissions the query was executed.

ORIGINAL_LOGIN
Pro: You see who created the connection.
Con: You don't know with which permissions the query was executed.
*/

-- http://www.differencebetween.net/business/difference-between-purchase-order-and-invoice/

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% BEGIN REPORTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- MS SQL Server:
-- list tables
-- select * from [database_name].information_schema.tables;