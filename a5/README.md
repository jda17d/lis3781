> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Jimmy Anderson

### Assignment 5 Requirements:

*Three Parts:*

1. Alter Estore database from Assignment 4
2. Populate database with min. 5 records (25 in sales)
3. Chapter Questions (Chp. 15)

#### README.md file should include the following items:

* SQL file used to create database
* Screenshot of populated tables
* Screenshot of ERD

#### Assignment Links:

*Assignment 5 SQL File:*
[SQL File](https://bitbucket.org/jda17d/lis3781/src/master/a5/docs/LIS3781JimmyAndersonA5.sql "Assignment 5 SQL File")

#### Assignment Screenshots:

| *Screenshot of A5 Business Rules*: |
| :----:  |
| ![A5 Business Rules](img/brules.png) |

| *Screenshot of A5 ERD*: |
| :----:  |
| ![A5 ERD](img/erd.png) |

| *Screenshot of Tables - Person/SlsRep/Customer*: | *Screenshot of Tables - Contact/Order/Region/State*: |
| :----:        |    :----:   |
| ![Person/SlsRep/Customer](img/tables-PersonSlsrepCustomer.png) | ![Contact/Order/Region/State](img/tables-ContactOrderRegionState.png) |

| *Screenshot of Tables - City/Store/Invoice/Vendor*: | *Screenshot of Tables - Product/Orderline/Payment/Phone*: |
| :----:        |    :----:   |
| ![City/Store/Invoice/Vendor](img/tables-CityStoreInvoiceVendor.png) | ![Product/Orderline/Payment/Phone](img/tables-ProductOrderlinePaymentPhone.png) |

| *Screenshot of Tables - ProductHist/Time/Sale/SrpHist*: |
| :----:  |
| ![ProductHist/Time/Sale/SrpHist](img/tables-ProducthistTimeSaleSrphist.png) |

#### Tutorial Links:

*A5 Part 1:*
[A5 Part 1 Tutorial](https://youtu.be/6r5X_ufnyhU "A5 Tutorial Part 1")

*A5 Part 2:*
[A5 Part 2 Tutorial](https://youtu.be/Ugvs6FjgQtI "A5 Tutorial Part 2")

*A5 Part 3:*
[A5 Part 3 Tutorial](https://youtu.be/NL0LL5U4E_E "A5 Tutorial Part 3")