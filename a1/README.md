> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Jimmy Anderson

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Chapter Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket repo links:
    a) this assignment and
    b) the completed tutorial (bitbucketstationlocations)

### A1 Database Business Rules:

Business RulesThe human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes:job description, length of employment, benefits,number ofdependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories mustbe tracked. 

Also, include the following business rules:

* Each employee may have one or more dependents.
* Each employee has only one job.
* Each job can be held by many employees.
* Many employees may receive many benefits.
* Many benefits may be selected by many employees(though, while they may not select anybenefits—any dependentsof employees may be on anemployee’s plan).
* Employee/Dependenttables must use suitable attributes (See Assignment Guidelines).
* Employee: SSN, DOB, start/end dates, salary;• Dependent:same information as their associated employee(though, not start/end dates),date added(as dependent),type of relationship: e.g., father, mother, etc.
* Job: title(e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
* Benefit: name(e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
* Plan: type(single, spouse, family), cost, election date(plans must be unique).
* Employee history: jobs, salaries, and benefit changes, as well as who made the change and why.
* Zero Filled data: SSN, zip codes (not phone numbers: US area codesnot below 201, NJ).
* *All* tables must include notes attribute.

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex1. SQL Solution
* git command w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository.
2. git status - List the files you've changed and those you still need to add or commit.
3. git add - Add one or more files to staging (index).
4. git commit - Commit any files youve added with git add, and also commit any files you've cahnged since then.
5. git push - Send changes to the master branch of your remote repository.
6. git pull - Fetch and merge changes on the remote server to your working directory.
7. git clone - Create a working copy of a local/remote repository.

#### Assignment Screenshots:

| *Screenshot of AMPPS running http://localhost* : |
|     :----:    |
| ![AMPPS Installation Screenshot](img/ampps.png) |

| *Screenshot of Entity Relationship Diagram (ERD)* : |
|     :----:    |
| ![Assignment 1 Entity Relationship Diagram](img/A1ERD.png) |

| *SQL Query #3* : |
|     :----:    |
| ![SQL Query Example 3](img/sqlex.png) |

#### Assignment Links:

*Assignment 1 - SQL File:*
[Assignment 1 - SQL File](https://bitbucket.org/jda17d/lis3781/src/master/a1/docs/lis3781_a1_solutions.sql "Assignment 1 SQL File")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jda17d/bitbucketstationlocations/ "Bitbucket Station Locations")
