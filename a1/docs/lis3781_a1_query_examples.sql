EX1
select emp_id, emp_fname, emp_lname,
CONCAT(emp_street, ", ", emp_city, ", ", emp_state, ", ", substring(emp_zip,1,5), '-', substring(emp_zip,6,4)) as address,
CONCAT('(', substring(emp_phone,1,3), ')',substring(emp_phone,4,3), '-', substring(emp_phone,7,4)) as phone_num,
CONCAT(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn, job_title
from job as j, employee as e
where j.job_id = e.job_id
order by emp_lname desc;

EX2 ??
select e.emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary, eht_notes
from employee e, emp_hist h, job j
where e.emp_id = h.emp_id
    and eht_job_id = j.job_id 
order by emp_id, eht_date;


EX3
select e.emp_fname, e.emp_lname, e.emp_dob, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), e.emp_dob)), "%Y")+0 
AS emp_age, d.dep_fname, d.dep_lname, d.dep_relation, d.dep_dob, 
DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), d.dep_dob)), "%Y")+0 AS dep_age
from employee as e, dependent as d
where d.emp_id = e.emp_id
order by e.emp_lname asc;

EX4
start transaction;
    select * from job;

    update job
    set job_title='owner'
    where job_id=1;

    select * from job;
commit;

EX5
drop procedure if exists insert_benefit;
DELIMITER //
create procedure insert_benefit()
begin
    select * from benefit ;

    insert into benefit
    (ben_name, ben_notes)
    values
    ('new benefit', 'testing');

    select * from benefit;
end //
DELIMITER ;

call insert_benefit();
drop procedure if exists insert_benefit;

EX6
select CONCAT(e.emp_lname, ", ", e.emp_fname) as employee,
CONCAT(substring(e.emp_ssn,1,3), '-', substring(e.emp_ssn,4,2), '-', substring(e.emp_ssn,6,4)) as emp_ssn,
CONCAT(e.emp_email) as email,
CONCAT(d.dep_lname, ", ", d.dep_fname) as dependents,
CONCAT(substring(d.dep_ssn,1,3), '-', substring(d.dep_ssn,4,2), '-', substring(d.dep_ssn,6,4)) as dep_ssn,
CONCAT(e.emp_street, ", ", e.emp_city, ", ", e.emp_state, ", ", substring(e.emp_zip,1,5), '-', substring(e.emp_zip,6,4)) as addresses,
CONCAT('(', substring(e.emp_phone,1,3), ')',substring(e.emp_phone,4,3), '-', substring(e.emp_phone,7,4)) as phone_num
from employee as e, dependent as d
where d.emp_id = e.emp_id
order by e.emp_lname asc;

EX7
