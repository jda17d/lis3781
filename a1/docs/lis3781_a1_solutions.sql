-- MySQL Script generated by MySQL Workbench
-- Sun Jan 17 19:44:45 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema jda17d
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `jda17d` ;

-- -----------------------------------------------------
-- Schema jda17d
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jda17d` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `jda17d` ;

-- -----------------------------------------------------
-- Table `jda17d`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jda17d`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jda17d`.`job` (
  `job_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NOT NULL,
  `job_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`),
  UNIQUE INDEX `job_id_UNIQUE` (`job_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jda17d`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jda17d`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jda17d`.`employee` (
  `emp_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_id` TINYINT UNSIGNED NOT NULL,
  `emp_ssn` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `emp_fname` VARCHAR(15) NOT NULL,
  `emp_lname` VARCHAR(30) NOT NULL,
  `emp_dob` DATE NOT NULL,
  `emp_start_date` DATE NOT NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_street` VARCHAR(30) NOT NULL,
  `emp_city` VARCHAR(20) NOT NULL,
  `emp_state` CHAR(2) NOT NULL,
  `emp_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `emp_phone` BIGINT UNSIGNED NOT NULL,
  `emp_email` VARCHAR(100) NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`emp_id`),
  UNIQUE INDEX `emp_ssn_UNIQUE` (`emp_ssn` ASC),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC),
  INDEX `fk_employee_job1_idx` (`job_id` ASC),
  CONSTRAINT `fk_employee_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `jda17d`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jda17d`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jda17d`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jda17d`.`dependent` (
  `dep_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `dep_added` DATE NOT NULL,
  `dep_ssn` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `dep_fname` VARCHAR(15) NOT NULL,
  `dep_lname` VARCHAR(30) NOT NULL,
  `dep_dob` DATE NOT NULL,
  `dep_relation` VARCHAR(20) NOT NULL,
  `dep_street` VARCHAR(30) NOT NULL,
  `dep_city` VARCHAR(20) NOT NULL,
  `dep_state` CHAR(2) NOT NULL,
  `dep_zip` INT(9) ZEROFILL UNSIGNED NOT NULL,
  `dep_phone` BIGINT UNSIGNED NOT NULL,
  `dep_email` VARCHAR(100) NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  UNIQUE INDEX `dep_id_UNIQUE` (`dep_id` ASC),
  UNIQUE INDEX `dep_ssn_UNIQUE` (`dep_ssn` ASC),
  INDEX `fk_dependent_employee_idx` (`emp_id` ASC),
  CONSTRAINT `fk_dependent_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `jda17d`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jda17d`.`benefit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jda17d`.`benefit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jda17d`.`benefit` (
  `ben_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NOT NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`),
  UNIQUE INDEX `ben_id_UNIQUE` (`ben_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jda17d`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jda17d`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jda17d`.`plan` (
  `pln_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `ben_id` TINYINT UNSIGNED NOT NULL,
  `pln_type` ENUM('single', 'spouse', 'family') NOT NULL,
  `pln_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pln_election_date` DATE NOT NULL,
  `pln_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pln_id`),
  UNIQUE INDEX `pln_id_UNIQUE` (`pln_id` ASC),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC),
  INDEX `fk_plan_benefit1_idx` (`ben_id` ASC),
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `jda17d`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_benefit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `jda17d`.`benefit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jda17d`.`emp_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jda17d`.`emp_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jda17d`.`emp_hist` (
  `eht_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NULL,
  `eht_date` DATETIME NOT NULL DEFAULT current_timestamp,
  `eht_type` ENUM('i', 'u', 'd') NOT NULL DEFAULT 'i',
  `eht_job_id` TINYINT UNSIGNED NOT NULL,
  `eht_emp_salary` DECIMAL(8,2) NOT NULL,
  `eht_usr_changed` VARCHAR(30) NOT NULL,
  `eht_reason` VARCHAR(45) NOT NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  UNIQUE INDEX `eht_id_UNIQUE` (`eht_id` ASC),
  INDEX `fk_emp_hist_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_emp_hist_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `jda17d`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `jda17d`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `jda17d`;
INSERT INTO `jda17d`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'accountant', NULL);
INSERT INTO `jda17d`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'cashier', NULL);
INSERT INTO `jda17d`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'manager', NULL);
INSERT INTO `jda17d`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'human resources', NULL);
INSERT INTO `jda17d`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'intern', 'temp');

COMMIT;


-- -----------------------------------------------------
-- Data for table `jda17d`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `jda17d`;
INSERT INTO `jda17d`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 2, 788234783, 'Bob', 'Holiday', '1975-04-08', '2014-06-11', NULL, 62352.23, '123 Road Rd.', 'Tallahassee', 'FL', 32311, 8505095555, 'Bob@hotmail.com', NULL);
INSERT INTO `jda17d`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 3, 728937592, 'Bill', 'Dozer', '1963-12-06', '2005-04-08', '2015-06-11', 46323.32, '352 Circle Ct.', 'Tallahassee', 'FL', 32303, 8505096732, 'Bill@aol..com', 'bad employee');
INSERT INTO `jda17d`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 5, 474574572, 'Sarah', 'Lee', '1996-05-11', '2001-05-10', NULL, 46323.56, '2579 Apple Rd.', 'Jacksonville', 'FL', 32783, 8503262362, 'Sarah@gmail.com', 'test');
INSERT INTO `jda17d`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 1, 932949773, 'Lilly', 'Frog', '2002-09-10', '2018-07-05', NULL, 54735.54, '257 Orange Av.', 'Orlando', 'FL', 32586, 8504364362, 'Lilly@yahoo.com', NULL);
INSERT INTO `jda17d`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 4, 723789563, 'Blake', 'Smith', '1984-05-05', '2015-02-06', NULL, 65735.67, '4690 Fly Dr.', 'Miami', 'FL', 78638, 8505087234, 'Smith@gmail.com', 'test');

COMMIT;


-- -----------------------------------------------------
-- Data for table `jda17d`.`dependent`
-- -----------------------------------------------------
START TRANSACTION;
USE `jda17d`;
INSERT INTO `jda17d`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 1, '2002-08-11', 392953750, 'Ted', 'Montana', '1978-08-11', 'brother', '123 Drive dr.', 'Tallahassee', 'FL', 32311, 8505095555, 'montana@gmail.com', NULL);
INSERT INTO `jda17d`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 4, '2011-05-10', 253262362, 'Rodger', 'Rabbit', '1997-09-03', 'husband', '892 Ace dr.', 'Miami', 'FL', 32306, 5095097843, 'rabbit@gmail.com', 'test');
INSERT INTO `jda17d`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 2, '2017-10-05', 437634734, 'Millly', 'Parks', '2005-11-04', 'wife', '2953 Bottom rd.', 'Orlando', 'FL', 32062, 8503262363, 'parks@gmail.com', NULL);
INSERT INTO `jda17d`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 3, '1999-09-10', 234234235, 'Maranda', 'Ships', '2007-08-02', 'wife', '992 Eye ct.', 'Tampa', 'FL', 32333, 6304603402, 'ships@aol.com', NULL);
INSERT INTO `jda17d`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 2, '2003-11-06', 347634734, 'Abby', 'Moon', '1995-01-07', 'daughter', '3567 Circle dr.', 'Tampa', 'FL', 32636, 4362759894, 'moon@yahoo.com', 'test');

COMMIT;


-- -----------------------------------------------------
-- Data for table `jda17d`.`benefit`
-- -----------------------------------------------------
START TRANSACTION;
USE `jda17d`;
INSERT INTO `jda17d`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Health', NULL);
INSERT INTO `jda17d`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Dentist', NULL);
INSERT INTO `jda17d`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Eye', 'test');
INSERT INTO `jda17d`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Emergency', NULL);
INSERT INTO `jda17d`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Complete', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `jda17d`.`plan`
-- -----------------------------------------------------
START TRANSACTION;
USE `jda17d`;
INSERT INTO `jda17d`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 2, 1, 'single', 235.36, '2011-08-10', 'test');
INSERT INTO `jda17d`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 4, 3, 'spouse', 4562.36, '2005-08-04', NULL);
INSERT INTO `jda17d`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 2, 3, 'family', 234.36, '2009-02-11', 'test');
INSERT INTO `jda17d`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 5, 2, 'family', 547.32, '1997-12-09', NULL);
INSERT INTO `jda17d`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 1, 5, 'single', 743.35, '2003-07-02', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `jda17d`.`emp_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `jda17d`;
INSERT INTO `jda17d`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 1, '2001-11-06', 'i', 1, 32352.43, 'manager', 'inserted new employee', 'test');
INSERT INTO `jda17d`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 3, '1998-10-03', 'u', 2, 34634.43, 'manager', 'updated employee address', NULL);
INSERT INTO `jda17d`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, NULL, '2007-01-08', 'd', 3, 54363.32, 'manager', 'employee quit', NULL);
INSERT INTO `jda17d`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, NULL, '2001-08-11', 'd', 4, 45754.63, 'manager', 'employee quit', 'test');
INSERT INTO `jda17d`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, NULL, '2009-08-02', 'd', 5, 23590.34, 'manager', 'employee quit', NULL);

COMMIT;

