EX1
select emp_id, emp_fname, emp_lname,
CONCAT(emp_street, ", ", emp_city, ", ", emp_state, ", ", substring(emp_zip,1,5), '-', substring(emp_zip,6,4)) as address,
CONCAT('(', substring(emp_phone,1,3), ')',substring(emp_phone,4,3), '-', substring(emp_phone,7,4)) as phone_num,
CONCAT(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn, job_title
from job as j, employee as e
where j.job_id = e.job_id
order by emp_lname desc;

EX2 ??
select e.emp_id, e.emp_fname, e.emp_lname, h.eht_date, h.eht_job_id, j.job_title, h.eht_emp_salary, h.eht_notes
from job as j, employee as e, emp_hist as h
where j.job_id = e.job_id
order by e.emp_id asc, eht_date;


EX3
select e.emp_fname, e.emp_lname, e.emp_dob, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), e.emp_dob)), "%Y")+0 AS emp_age, d.dep_fname, d.dep_lname, d.dep_relation, d.dep_dob, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), d.dep_dob)), "%Y")+0 AS dep_age
from employee as e, dependent as d
where d.emp_id = e.emp_id
order by e.emp_lname asc;

EX4
start transaction;

select job_title from job where job_id = 1 for update;

update job set job_title = "owner" where job_id = 1;

select job_title from job where job_id = 1;

EX5
CREATE PROCEDURE add_benefit
AS
select * from benefit
INSERT INTO benefit (ben_name, ben_notes)
VALUES ('new benefit', 'testing')
GO;

EXEC add_benefit;

EX6
select CONCAT(e.emp_lname, ", ", e.emp_fname) as employee,
CONCAT(substring(e.emp_ssn,1,3), '-', substring(e.emp_ssn,4,2), '-', substring(e.emp_ssn,6,4)) as emp_ssn,
CONCAT(e.emp_email) as email,
CONCAT(d.dep_lname, ", ", d.dep_fname) as dependents,
CONCAT(substring(d.dep_ssn,1,3), '-', substring(d.dep_ssn,4,2), '-', substring(d.dep_ssn,6,4)) as dep_ssn,
CONCAT(e.emp_street, ", ", e.emp_city, ", ", e.emp_state, ", ", substring(e.emp_zip,1,5), '-', substring(e.emp_zip,6,4)) as addresses,
CONCAT('(', substring(e.emp_phone,1,3), ')',substring(e.emp_phone,4,3), '-', substring(e.emp_phone,7,4)) as phone_num
from employee as e, dependent as d
where d.emp_id = e.emp_id
order by e.emp_lname asc;

EX7