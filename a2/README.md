> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Jimmy Anderson

### Assignment 2 Requirements:

*Three Parts:*

1. Create database and populate records with SQL (no workbench erd)
2. Create two users on the local database with specific privileges
3. Chapter Questions (Chp. 11)

#### README.md file should include the following items:

* Screenshots of SQL Code used to create/populate database
* Screenshot of populated database tables

#### Assignment Screenshots:

| *Screenshot of SQL code used to create/populate database - 1/2*: | *Screenshot of SQL code used to create/populate database - 2/2*: |
| :----:        |    :----:   |
| ![SQL code 1/2](img/sqlcode1.png) | ![SQL code 2/2](img/sqlcode2.png) |

| *Screenshot of Populated database tables - Company & Customer*: |
| :----:        |
| ![Populated Tables](img/databasepopulated.png) |