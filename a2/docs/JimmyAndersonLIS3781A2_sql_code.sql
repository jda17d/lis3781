/*
A character set is a set of symbols and encodings.
A collation is a set of rules for comparing characters in a character set.

Suppose that we have an alphabet with four letters: "A", "B", "a", "b".
We give each letter a number: "A" = 0, "B" = 1, "a" = 2, "b" = 3.
The letter "A" is a symbol, the number 0 is the encoding for "A",
and the combination of all four letters and their encodings is a character set.

Suppose that we want to compare two string values, "A" and "B".
The simplest way to do this is to look at the encodings: 0 for "A" and 1 for "B".
Because 0 is less than 1, we say "A" is less than "B". What we've just done is apply a collation to our character set.
The collation is a set of rules (only one rule in this case): "compare the encodings."
We call this simplest of all possible collations a binary collation.

http://dev.mysql.com/doc/refman/5.5/en/charset.html
*/

-- set foreign_key_checks=0;

drop database if exists jda17d;
create database if not exists jda17d;
use jda17d;

-- -----------------------------------------------
-- Table company
-- -----------------------------------------------
DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
    cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
    cmp_street VARCHAR(30) NOT NULL,
    cmp_city VARCHAR(30) NOT NULL,
    cmp_state CHAR(2) NOT NULL,
    cmp_zip INT(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes',
    cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
    cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
    cmp_email VARCHAR(100) NULL,
    cmp_url VARCHAR(100) NULL,
    cmp_notes VARCHAR(255) NULL,
    PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'Non-Profit-Corp','732 Road Rd.','Tampa','FL','123456789','8505551234','12345678.00',null,'http://www.jimmydanderson850.com','company notes 1'),
(null,'S-Corp','245 Circle Ct.','Tallahassee','FL','728536486','8509286475','74829283.00',null,'http://www.jimmydanderson850.com','company notes 2'),
(null,'LLC','520 Lemon Av.','Miami','FL','926548620','8509278273','82563795.00',null,'http://www.jimmydanderson850.com','company notes 3'),
(null,'Partnership', '7982 Old House St.','Dallas','TX','735217036','6709358946','77508293.00',null,'http://www.jimmydanderson850.com','company notes 4'),
(null,'C-Corp','6892 Bane Rd.','Jacksonville','FL','936837558','8507239643','24895374.00',null,'http://www.jimmydanderson850.com','company notes 5');

SHOW WARNINGS;

-- ---------------------------------------
-- Table customer
-- ---------------------------------------
DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
    cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_id INT UNSIGNED NOT NULL,
    cus_ssn binary(64) not null,
    cus_salt binary(64) not null COMMENT '*only* demo purposes - do *NOT* use *salt* in the name!',
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first VARCHAR(15) NOT NULL,
    cus_last VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NULL,
    cus_city VARCHAR(30) NULL,
    cus_state CHAR(2) NULL,
    cus_zip int(9) unsigned ZEROFILL NULL,
    cus_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
    cus_email VARCHAR(100) NULL,
    cus_balance DECIMAL(7,2) unsigned NULL COMMENT '12,345.67',
    cus_tot_sales DECIMAL(7,2) unsigned NULL,
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (cus_id),

    UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
    INDEX idx_cmp_id (cmp_id ASC),

/*
Comment CONSTRAINT line to demo DBMS auto value when *not* using "constraint" option for foreign keys, then...
SHOW CREATE TABLE customer;
*/
CONSTRAINT fk_customer_company
    FOREIGN KEY (cmp_id)
    REFERENCES company (cmp_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

-- salting and hashing sensitive data (e.g., SSN). Normally, *each* record would recieve unique random salt!
set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)),@salt,'Discount','Ted','Durber','7689 Road Rd.','Tallahassee','FL','085703412','8505551234','testmail1@gmail.com','8391.62','78372.46','customer notes 1'),
(null,4,unhex(SHA2(CONCAT(@salt, 001456789),512)),@salt,'Impulse','Alice','Chains','7835 Circle Dr.','Tampa','FL','123456789','8507778293','testmail2@gmail.com','632.95','54745.45','customer notes 2'),
(null,3,unhex(SHA2(CONCAT(@salt, 002456789),512)),@salt,'Need-Based','Frank','Ocean','835 Tall Ln.','Dallas','TX','987654321','8530298372','testmail3@gmail.com','7849.43','58568.23','customer notes 3'),
(null,5,unhex(SHA2(CONCAT(@salt, 003456789),512)),@salt,'Wandering','Bill','Dozer','243 Data Rd.','Jacksonville','FL','109384756','8506378295','testmail4@gmail.com','9823.46','98943.46','customer notes 4'),
(null,1,unhex(SHA2(CONCAT(@salt, 004456789),512)),@salt,'Loyal','Ellie','Smith','748 Small Av.','Miami','FL','564382910','6479387461','testmail5@gmail.com','943.28','3245.85','customer notes 5');

SHOW WARNINGS;
-- set foreign_key_checks=1;

select * from company;
select * from customer;