<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="LIS4381 Project 1 Business Card application in android studio.">
		<meta name="author" content="Jimmy Anderson">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify" style="font-size: 20px">
					<strong>Description:</strong> The expected norm for
				</p>

				<div style="float: left;">
				<h4><strong>Business Card Splash Screen</strong></h4>
				<img src="img/businesscard1.png" class="img-responsive center-block" alt="Business Card App Splash Screen">
</div>

<div style="float: right;">
				<h4><strong>Business Card Info Screen</strong></h4>
				<img src="img/businesscard2.png" class="img-responsive center-block" alt="Business Card App Info Screen">
</div>
<div style="margin-top: 900px;">
				<?php include_once "global/footer.php"; ?>
<div>
			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
