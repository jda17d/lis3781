> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Jimmy Anderson

### Project 1 Requirements:

*Three Parts:*

1. Create database to track city courts
2. Chapter questions (Chp. 13)
3. Create desired reports

#### README.md file should include the following items:

* Screenshot of Court Database ERD
* Screenshot of Court Database Tables - Populated
* MWB file and SQL file to create database

#### Business Rules

| *City Court Database - Business Rules*: |
| :----:        |
| ![Business Rules](img/rules.png) |

#### Assignment Links:

*Project 1 Court Database MWB File:*
[Project 1 MWB File](https://bitbucket.org/jda17d/lis3781/src/master/p1/docs/JimmyAnderson_LIS3781_P1_ERD.mwb "Project 1 MWB File")

*Project 1 Court Database SQL File:*
[Project 1 SQL File](https://bitbucket.org/jda17d/lis3781/src/master/p1/docs/JimmyAnderson_LIS3781_P1_solutions.sql "Project 1 SQL File")

#### Assignment Screenshots:

| *Screenshot of Court Database - ERD*: |
| :----:        |
| ![Court Database ERD](img/ERD.png) |

| *Screenshot of Court Database Table - Person*: |
| :----:        |
| ![Court Database Table Person](img/table.png) |

| *Screenshot of Court Database Tables - Phone/Client/Attorney*: | *Screenshot of Court Database Tables - Specialty/Bar*: |
| :----:        |    :----:   |
| ![Court Database Tables](img/table1.png) | ![Court Database Tables](img/table2.png) |

| *Screenshot of Court Database Tables - Court/Judge/Judge_Hist*: |
| :----:        |
| ![Court Database Tables](img/table3.1.png) |

| *Screenshot of Court Database Tables - Case/Assignment*: |
| :----:        |
| ![Court Database Tables](img/table4.1.png) |